//
//  ProfileViewController.swift
//  BA_IG_Gallery
//
//  Created by Baris Atamer on 10/22/17.
//  Copyright © 2017 Baris Atamer. All rights reserved.
//

import UIKit
import AlamofireImage
import ElasticTransition
import Reachability
import Rswift
import Floaty

class ProfileViewController: UIViewController {

    @IBOutlet weak var imageViewProfilePicture: UIImageView!
    @IBOutlet weak var labelUserName: UILabel!
    @IBOutlet weak var labelPostsCount: UILabel!
    @IBOutlet weak var labelFollowerCount: UILabel!
    @IBOutlet weak var labelFollowingCount: UILabel!
    @IBOutlet weak var buttonOpenGallery: DuckMouth!
    
    var transition = ElasticTransition()
    let reachability = Reachability()!

    // MARK: - Init
    override func loadView() {
        super.loadView()
        
        // Start reachability
        do {
            try reachability.startNotifier()
        } catch {
            print("Unable to start notifier")
        }
        
        // Set open gallery button title & callback
        buttonOpenGallery.setButtonTitle(title: R.string.main.openGallery())
        buttonOpenGallery.animEnded = {
            self.eventOpenGallery()
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        navigationController?.setNavigationBarHidden(true, animated: false)
        
        styleViews()
        
        // Get user if connection is available
        if reachability.connection != .none {
           setUserInfo()
        }
        // Get user when connection becomes available
        reachability.whenReachable = { reachability in
            self.setUserInfo()
        }
        
        // Set the profile picture with the saved url
        if let url = UserDefaults.standard.url(forKey: "avatar") {
            setUserProfilePicture(url: url)
        }
        
        // Add the fab menu at bottom-right corner
        addMenuButton()
    }
    
    func addMenuButton() {
        let floaty = Floaty()
        floaty.buttonImage = R.image.ic_menu()
        floaty.plusColor = UIColor.white
        floaty.itemButtonColor = .orange
        floaty.buttonColor = .orange
        floaty.addItem(R.string.main.logout(), icon: R.image.ic_logout(), titlePosition: .left) { (item) in
            self.eventLogout()
        }
        self.view.addSubview(floaty)
    }
    
    func styleViews() {
        imageViewProfilePicture.addShadow()
        imageViewProfilePicture.addStroke()
        labelUserName.addShadow()
    }
    
    // MARK: - Button events
    func eventOpenGallery() {
        transition.edge = .right
        transition.transformType = .translateMid
        transition.startingPoint = buttonOpenGallery.center
        let modalViewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "Gallery") as! GalleryViewController
        present(modalViewController, animated: true, completion: nil)
    }
    
    func eventLogout() {
        InstagramEngine.sharedInstance.logout()
        UserDefaults.standard.removeObject(forKey: "avatar")
        navigationController?.popViewController(animated: true)
    }
    
    // MARK: - Other
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        let vc = segue.destination
        vc.transitioningDelegate = transition
        vc.modalPresentationStyle = .custom
    }
    
    // MARK: - Fetching
    func setUserInfo() {
        // Fetch user info
        InstagramEngine.sharedInstance.fetchUserInfo(completion: { [weak self] result in
            
            switch result {
            case .success(let userInfo):
                
                // Set profile picture
                if let imgUrl = URL(string: userInfo.profilePicture!) {
                    self?.setUserProfilePicture(url: imgUrl)
                }
                // Set username
                self?.labelUserName.text = "@" + userInfo.username!
                
                self?.labelPostsCount.alpha = 0
                self?.labelFollowerCount.alpha = 0
                self?.labelFollowingCount.alpha = 0
                
                UIView.animate(withDuration: 1.25, delay: 0.5, usingSpringWithDamping: 0.1, initialSpringVelocity: 1.5, options: [.curveEaseInOut], animations: {
                    
                    self?.labelPostsCount.alpha = 1
                    self?.labelFollowerCount.alpha = 1
                    self?.labelFollowingCount.alpha = 1
                })
                
                self?.labelPostsCount.text = String(userInfo.mediaCount!)
                self?.labelFollowerCount.text = String(userInfo.followedByCount!)
                self?.labelFollowingCount.text = String(userInfo.followsCount!)
                
                return
                
            case .failure(let error):
                if let err = error {
                    if err.rawValue == 401 {
                        self?.eventLogout()
                    }
                }
                return
            }
        })
    }
    
    func setUserProfilePicture(url: URL) {
        self.imageViewProfilePicture.af_setImage(withURL: url, placeholderImage: nil, filter: CircleFilter(), imageTransition: UIImageView.ImageTransition.crossDissolve(0.9), runImageTransitionIfCached: false, completion: { (response) in
            UserDefaults.standard.set(url, forKey: "avatar")
        })
    }
}
