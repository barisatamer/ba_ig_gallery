//
//  PostViewController.swift
//  BA_IG_Gallery
//
//  Created by Baris Atamer on 10/24/17.
//  Copyright © 2017 Baris Atamer. All rights reserved.
//

import UIKit
import AlamofireImage
import ElasticTransition

class PostViewController: ElasticModalViewController {

    @IBOutlet weak var imageViewPost: UIImageView!
    
    var post: Post?
    var placeholderImage: UIImage?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        loadImage()
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }
    
    func loadImage() {
        guard let urlString = post?.imageStandardResolution.url,
            let url = URL(string: urlString) else {
                return
        }
        
        imageViewPost.af_setImage(withURL: url, placeholderImage: placeholderImage, imageTransition: UIImageView.ImageTransition.crossDissolve(0.5), runImageTransitionIfCached: false, completion: { (response) in
        })
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        imageViewPost.frame = view.bounds
    }
}
