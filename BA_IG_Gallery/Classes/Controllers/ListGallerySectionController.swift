//
//  ListGallerySectionController.swift
//  instagram_posts_test
//
//  Created by Baris Atamer on 10/22/17.
//  Copyright © 2017 Baris Atamer. All rights reserved.
//

/**
 Copyright (c) 2016-present, Facebook, Inc. All rights reserved.
 
 The examples provided by Facebook are for non-commercial testing and evaluation
 purposes only. Facebook reserves all rights not expressly granted.
 
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
 FACEBOOK BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN
 ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

import UIKit
import IGListKit
import Alamofire

protocol ListGallerySectionControllerDelegate: class {
    func itemTapped(_ sectionController: ListGallerySectionController, item: Post?, placeholder: UIImage?)
}

final class ListGallerySectionController: ListSectionController, ListWorkingRangeDelegate {
    
    private var post: PostDiffable?
    
    private var height: Int?
    private var downloadedImage: UIImage?
    private var task: URLSessionDataTask?
    
    weak var delegate: ListGallerySectionControllerDelegate?
    
    deinit {
        task?.cancel()
    }
    
    override init() {
        super.init()
        workingRangeDelegate = self
    }
    
    override func numberOfItems() -> Int {
        return 1
    }
    
    override func sizeForItem(at index: Int) -> CGSize {
        let width: CGFloat = collectionContext?.containerSize.width ?? 0
        return CGSize(width: width/3, height: width/3)
    }
    
    override func cellForItem(at index: Int) -> UICollectionViewCell {
        let cellClass: AnyClass = ImageCell.self
        
        let cell = collectionContext!.dequeueReusableCell(of: cellClass, for: self, at: index)
        if let cell = cell as? ImageCell {
            cell.setImage(image: downloadedImage)
        }
        return cell
    }
    
    override func didUpdate(to object: Any) {
        self.post = object as? PostDiffable
    }
    
    // MARK: ListWorkingRangeDelegate
    
    func listAdapter(_ listAdapter: ListAdapter, sectionControllerWillEnterWorkingRange sectionController: ListSectionController) {
        guard downloadedImage == nil,
            let urlString = post?.post.imageThumbnail.url,
            let url = URL(string: urlString)
            else { return }
        
        Alamofire.request(url).responseImage { response in
            if let image = response.result.value {
                self.downloadedImage = image
                if let cell = self.collectionContext?.cellForItem(at: 0, sectionController: self) as? ImageCell {
                    cell.setImage(image: image)
                }
            }
        }
    }
    
    func listAdapter(_ listAdapter: ListAdapter, sectionControllerDidExitWorkingRange sectionController: ListSectionController) {}
    
    
    override func didSelectItem(at index: Int) {
        delegate?.itemTapped(self, item: post?.post, placeholder: downloadedImage)
    }
    
}


