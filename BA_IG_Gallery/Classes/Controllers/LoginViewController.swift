//
//  ViewController.swift
//  BA_IG_Gallery
//
//  Created by Baris Atamer on 10/22/17.
//  Copyright © 2017 Baris Atamer. All rights reserved.
//

import UIKit
import Reachability
import Rswift

class LoginViewController: UIViewController {

    @IBOutlet var backgroundView: GradientView!
    @IBOutlet weak var duckFaceLoginView: DuckFace!
    
    var instagramLoginView: InstagramLoginViewController!
    
    let reachability = Reachability()!
    
    override func loadView() {
        super.loadView()
        
        // Start reachability
        do {
            try reachability.startNotifier()
        } catch {
            print("Unable to start notifier")
        }
        
        navigationController?.setNavigationBarHidden(true, animated: false)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.duckFaceLoginView.mouth.setButtonTitle(title: R.string.main.connectWithInstagram())
        
        reachability.whenReachable = { reachability in
            self.duckFaceLoginView.mouth.setButtonTitle(title: R.string.main.connectWithInstagram())
        }
        reachability.whenUnreachable = { _ in
            self.duckFaceLoginView.mouth.setButtonTitle(title: R.string.main.connectionProblem())
        }
        
        self.duckFaceLoginView.mouth.animEnded = {
            self.openInstagramLoginView()
        }
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        backgroundView.animateGradient()
    }
    
    func openInstagramLoginView() {
        if reachability.connection != .none {
            // Create Instagram oAuth Web View
            instagramLoginView = InstagramLoginViewController()
            instagramLoginView.authURL = InstagramEngine.sharedInstance.authUrl()
            instagramLoginView.delegate = self
            let navVC = UINavigationController(rootViewController: instagramLoginView)
            present(navVC, animated: true)
        } else {
            self.duckFaceLoginView.mouth.setButtonTitle(title: R.string.main.connectionProblem())
        }
    }
}

// MARK: - Instagram Auth Delegate
extension LoginViewController: InstagramAuthDelegate {
    func authControllerDidFinish(accessToken: String?, error: NSError?) {
        // Push the Profile view controller
        if let vc = storyboard?.instantiateViewController(withIdentifier: "Profile") as? ProfileViewController {
            navigationController?.pushViewController(vc, animated: true)
        }
    }
}
