//
//  GalleryViewController.swift
//  BA_IG_Gallery
//
//  Created by Baris Atamer on 10/24/17.
//  Copyright © 2017 Baris Atamer. All rights reserved.
//

import UIKit
import IGListKit
import ElasticTransition
import Reachability

class GalleryViewController: ElasticModalViewController, ListAdapterDataSource, UIScrollViewDelegate, ListGallerySectionControllerDelegate {
    
    @IBOutlet weak var labelTitle: UILabel!
    lazy var adapter: ListAdapter = {
        return ListAdapter(updater: ListAdapterUpdater(), viewController: self, workingRangeSize: 2)
    }()
    
    let collectionView: UICollectionView = {
        let layout = ListCollectionViewLayout.init(stickyHeaders: false, scrollDirection: UICollectionViewScrollDirection.vertical, topContentInset: 60, stretchToEdge: true)
        let collectionView = UICollectionView(frame: .zero, collectionViewLayout: layout)
        collectionView.backgroundColor = UIColor.clear
        return collectionView
    }()

    var loading = false
    let spinToken = "spinner"
    
    var posts: [PostDiffable] = [PostDiffable]()
    
    var transition = ElasticTransition()
    let reachability = Reachability()!
    
    @IBOutlet weak var labelNoConnection: UILabel!
    
    func getRandomColor() -> UIColor{
        let randomRed:CGFloat = CGFloat(drand48())
        let randomGreen:CGFloat = CGFloat(drand48())
        let randomBlue:CGFloat = CGFloat(drand48())
        return UIColor(red: randomRed, green: randomGreen, blue: randomBlue, alpha: 1.0)
    }
    
    // MARK: - Init
    
    override func loadView() {
        super.loadView()
        
        // Start reachability
        do {
            try reachability.startNotifier()
        } catch {
            print("Unable to start notifier")
        }
        
        // Set a random background color
        view.backgroundColor = getRandomColor()
        
        // Reachability check
        if reachability.connection != .none {
            fetchRecentPosts()
        } else {
            labelNoConnection.text = R.string.main.connectionProblem() + "☠"
            labelNoConnection.isHidden = false
        }
        
        // Set the title string with localization
        labelTitle.text = R.string.main.myGallery()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        view.addSubview(collectionView)
        adapter.collectionView = collectionView
        adapter.dataSource = self
        adapter.scrollViewDelegate = self
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()

        let screenSize = UIScreen.main.bounds.size
        var frame = collectionView.frame
        frame = CGRect(x: 0, y: 80, width: screenSize.width, height: screenSize.height - 80)
        collectionView.frame = frame
        labelTitle.frame = CGRect(x: 0, y: 35, width: screenSize.width, height: 24)
        
    }
    
    // MARK: - Fetch
    
    func fetchRecentPosts() {
        self.loading = true
        InstagramEngine.sharedInstance.fetchRecentPost(completion: { [weak self] result in
            switch result {
            case .success(let resultPosts):
                
                for post in resultPosts {
                    self?.posts.append(PostDiffable(post: post))
                }
                
                self?.loading = false
                self?.adapter.performUpdates(animated: true, completion: nil)
                
                return
            case .failure(_):
                return
            }
        })
    }
    
    // MARK: - Button events
    
    @IBAction func eventClose(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    
    // MARK: - ListAdapterDataSource
    
    func objects(for listAdapter: ListAdapter) -> [ListDiffable] {
        
        var objects = posts as [ListDiffable]
        
        if loading {
            objects.append(spinToken as ListDiffable)
        }
        
        return objects
    }
    
    func listAdapter(_ listAdapter: ListAdapter, sectionControllerFor object: Any) -> ListSectionController {
        if let obj = object as? String, obj == spinToken {
            return spinnerSectionController()
        } else {
            let sectionController = ListGallerySectionController()
            sectionController.delegate = self
            return sectionController
        }
    }
    
    func emptyView(for listAdapter: ListAdapter) -> UIView? {
        return nil
    }
    
    // MARK: UIScrollViewDelegate
    
    func scrollViewWillEndDragging(_ scrollView: UIScrollView,
                                   withVelocity velocity: CGPoint,
                                   targetContentOffset: UnsafeMutablePointer<CGPoint>) {
        
        let distance = scrollView.contentSize.height - (targetContentOffset.pointee.y + scrollView.bounds.height)
        
        if !loading && distance < 200 && velocity.y > 0 {
            loading = true
            adapter.performUpdates(animated: true, completion: nil)
            fetchRecentPosts()
        }
    }
    
    func itemTapped(_ sectionController: ListGallerySectionController, item: Post?, placeholder: UIImage?) {
        
        if let vc = storyboard?.instantiateViewController(withIdentifier: "Post") as? PostViewController {
            vc.post = item
            vc.placeholderImage = placeholder
            present(vc, animated: true, completion: nil)
        }
        
    }
    
    // MARK: - Other
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        let vc = segue.destination
        vc.transitioningDelegate = transition
        vc.modalPresentationStyle = .custom
    }
}

