//
//  GradientView.swift
//  BA_IG_Gallery
//
//  Created by Baris Atamer on 10/24/17.
//  Copyright © 2017 Baris Atamer. All rights reserved.
//

import UIKit

class GradientView: UIView {

    let gradient = CAGradientLayer()
    var gradientSet = [[CGColor]]()
    var currentGradient: Int = 0
    
    @IBInspectable var gradientOne: UIColor = UIColor(red: 48/255, green: 62/255, blue: 103/255, alpha: 1)
    @IBInspectable var gradientTwo: UIColor = UIColor(red: 244/255, green: 88/255, blue: 53/255, alpha: 1)
    @IBInspectable var gradientThree:UIColor = UIColor(red: 196/255, green: 70/255, blue: 107/255, alpha: 1)
    
    // beginning of the nib unarchiving process
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        initialize()
    }
    
    // end of the nib unarchiving process
    override func awakeFromNib() {
        super.awakeFromNib()
        
        initialize()
    }
    
    func initialize() {
        gradientSet.append([gradientOne.cgColor, gradientTwo.cgColor])
        gradientSet.append([gradientTwo.cgColor, gradientThree.cgColor])
        gradientSet.append([gradientThree.cgColor, gradientOne.cgColor])
        
        gradient.colors = gradientSet[currentGradient]
        gradient.startPoint = CGPoint(x:0, y:0)
        gradient.endPoint = CGPoint(x:1, y:1)
        gradient.drawsAsynchronously = true
        layer.insertSublayer(gradient, at: 0)

        animateGradient()
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        gradient.frame = self.bounds
    }
    
    func animateGradient() {
        
        
        if currentGradient < gradientSet.count - 1 {
            currentGradient += 1
        } else {
            currentGradient = 0
        }
        
        let gradientChangeAnimation = CABasicAnimation(keyPath: "colors")
        gradientChangeAnimation.duration = 5.0
        gradientChangeAnimation.toValue = gradientSet[currentGradient]
        gradientChangeAnimation.fillMode = kCAFillModeForwards
        gradientChangeAnimation.isRemovedOnCompletion = false
        gradientChangeAnimation.delegate = self
        gradient.add(gradientChangeAnimation, forKey: "colorChange")
        
        
    }

}

// MARK: - CAAnimationDelegate
extension GradientView: CAAnimationDelegate {
    func animationDidStop(_ anim: CAAnimation, finished flag: Bool) {
        if flag {
            gradient.colors = gradientSet[currentGradient]
            animateGradient()
        }
    }
}
