//
//  DuckEye.swift
//  BA_IG_Gallery
//
//  Created by Baris Atamer on 10/25/17.
//  Copyright © 2017 Baris Atamer. All rights reserved.
//

import UIKit

class DuckEye: UIView {

    // MARK: - Init
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupComponents()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setupComponents()
    }
    
    private func setupComponents() {
        backgroundColor = UIColor.clear
        clipsToBounds = false
    }
    
    // MARK: - Draw
    override func draw(_ rect: CGRect) {
        
        drawEye(frame: rect)
    }

    func drawEye(frame: CGRect = CGRect(x: 38, y: 95, width: 110, height: 110)) {
        
        // This non-generic function dramatically improves compilation times of complex expressions.
        func fastFloor(_ x: CGFloat) -> CGFloat { return floor(x) }
        
        //// Color Declarations
        let fillColor = UIColor(red: 0.000, green: 0.000, blue: 0.000, alpha: 1.000)
        
        //// Subframes
        let copiedObjectsGroup: CGRect = CGRect(x: frame.minX + fastFloor(frame.width * 0.00000 + 0.5), y: frame.minY + fastFloor(frame.height * -0.00000 - 0.5) + 1, width: fastFloor(frame.width * 0.99819 - 0.3) - fastFloor(frame.width * 0.00000 + 0.5) + 0.8, height: fastFloor(frame.height * 0.99819 - 0.3) - fastFloor(frame.height * -0.00000 - 0.5) - 0.2)
        
        // Drawing white eye ball
        let bezierPath = UIBezierPath()
        bezierPath.move(to: CGPoint(x: copiedObjectsGroup.minX + 0.50000 * copiedObjectsGroup.width, y: copiedObjectsGroup.minY + 1.00000 * copiedObjectsGroup.height))
        bezierPath.addCurve(to: CGPoint(x: copiedObjectsGroup.minX + 0.00000 * copiedObjectsGroup.width, y: copiedObjectsGroup.minY + 0.50000 * copiedObjectsGroup.height), controlPoint1: CGPoint(x: copiedObjectsGroup.minX + 0.22386 * copiedObjectsGroup.width, y: copiedObjectsGroup.minY + 1.00000 * copiedObjectsGroup.height), controlPoint2: CGPoint(x: copiedObjectsGroup.minX + 0.00000 * copiedObjectsGroup.width, y: copiedObjectsGroup.minY + 0.77614 * copiedObjectsGroup.height))
        bezierPath.addCurve(to: CGPoint(x: copiedObjectsGroup.minX + 0.50000 * copiedObjectsGroup.width, y: copiedObjectsGroup.minY + 0.00000 * copiedObjectsGroup.height), controlPoint1: CGPoint(x: copiedObjectsGroup.minX + 0.00000 * copiedObjectsGroup.width, y: copiedObjectsGroup.minY + 0.22386 * copiedObjectsGroup.height), controlPoint2: CGPoint(x: copiedObjectsGroup.minX + 0.22386 * copiedObjectsGroup.width, y: copiedObjectsGroup.minY + 0.00000 * copiedObjectsGroup.height))
        bezierPath.addCurve(to: CGPoint(x: copiedObjectsGroup.minX + 1.00000 * copiedObjectsGroup.width, y: copiedObjectsGroup.minY + 0.50000 * copiedObjectsGroup.height), controlPoint1: CGPoint(x: copiedObjectsGroup.minX + 0.77614 * copiedObjectsGroup.width, y: copiedObjectsGroup.minY + 0.00000 * copiedObjectsGroup.height), controlPoint2: CGPoint(x: copiedObjectsGroup.minX + 1.00000 * copiedObjectsGroup.width, y: copiedObjectsGroup.minY + 0.22386 * copiedObjectsGroup.height))
        bezierPath.addCurve(to: CGPoint(x: copiedObjectsGroup.minX + 0.50000 * copiedObjectsGroup.width, y: copiedObjectsGroup.minY + 1.00000 * copiedObjectsGroup.height), controlPoint1: CGPoint(x: copiedObjectsGroup.minX + 1.00000 * copiedObjectsGroup.width, y: copiedObjectsGroup.minY + 0.77614 * copiedObjectsGroup.height), controlPoint2: CGPoint(x: copiedObjectsGroup.minX + 0.77614 * copiedObjectsGroup.width, y: copiedObjectsGroup.minY + 1.00000 * copiedObjectsGroup.height))
        bezierPath.close()
        UIColor.white.setFill()
        bezierPath.fill()
        
        
        //// Drawing eye pupil
        let ovalPath = UIBezierPath(ovalIn: CGRect(x: copiedObjectsGroup.minX + fastFloor(copiedObjectsGroup.width * 0.43306 - 0.05) + 0.55, y: copiedObjectsGroup.minY + fastFloor(copiedObjectsGroup.height * 0.18944 - 0.3) + 0.8, width: fastFloor(copiedObjectsGroup.width * 0.84289 - 0.05) - fastFloor(copiedObjectsGroup.width * 0.43306 - 0.05), height: fastFloor(copiedObjectsGroup.height * 0.59927 - 0.3) - fastFloor(copiedObjectsGroup.height * 0.18944 - 0.3)))
        fillColor.setFill()
        ovalPath.fill()
    }

    
}
