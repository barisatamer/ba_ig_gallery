//
//  DuckMouth.swift
//  BA_IG_Gallery
//
//  Created by Baris Atamer on 10/25/17.
//  Copyright © 2017 Baris Atamer. All rights reserved.
//

import UIKit

class DuckMouth: UIView {

    typealias AnimEnded = () -> Void
    var animEnded: AnimEnded?
    
    @IBInspectable var overshootAmount : CGFloat = 20
    @IBInspectable var fontSize: CGFloat = 15
    @IBInspectable var strokeWidth: CGFloat = 10
    
    // Control points for animation
    private let topControlPointView = UIView()
    private let bottomControlPointView = UIView()
    
    // Shape for drawing animated background
    private let elasticShape = CAShapeLayer()
    
    // Button title
    private let label: UILabel = UILabel()
    
    private lazy var displayLink : CADisplayLink = {
        let displayLink = CADisplayLink(target: self, selector: #selector(updateLoop))
        displayLink.add(to: RunLoop.current, forMode: RunLoopMode.commonModes)
        return displayLink
    }()
    
    // MARK: - Init
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupComponents()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override func awakeFromNib() {
        setupComponents()
    }
    
    private func setupComponents() {
        
        for controlPoint in [topControlPointView, bottomControlPointView] {
                                addSubview(controlPoint)
                                controlPoint.frame = CGRect(x: 0.0, y: 0.0, width: 5.0, height: 5.0)
                                controlPoint.backgroundColor = UIColor.clear
        }
        positionControlPoints()
        
        backgroundColor = UIColor.clear
        clipsToBounds = false
        
        elasticShape.fillColor = UIColor.orange.cgColor
        let strokeColor2 = UIColor(red: 1.000, green: 1.000, blue: 1.000, alpha: 1.000)
        elasticShape.strokeColor = strokeColor2.cgColor
        elasticShape.path = UIBezierPath(rect: self.bounds).cgPath
        elasticShape.lineWidth = strokeWidth
        layer.addSublayer(elasticShape)
        
        addLabel()
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        label.frame = self.bounds
        
        positionControlPoints()
        elasticShape.path = bezierPathForControlPoints()
    }
    
    private func addLabel() {
        label.textAlignment = .center
        label.font = UIFont (name: "HelveticaNeue-Bold", size: fontSize)
        print("\(fontSize)")
        label.textColor = UIColor.white
        insertSubview(label, at: 10)
    }
    
    func setButtonTitle(title: String) {
        label.text = title
    }
    
    // MARK: - Drawing
    
    private func positionControlPoints(){
        topControlPointView.center = CGPoint(x: bounds.midX, y: 0.0)
        bottomControlPointView.center = CGPoint(x:bounds.midX, y: bounds.maxY)
    }
    
    private func bezierPathForControlPoints()->CGPath {
        let top = topControlPointView.layer.presentation()?.position ?? CGPoint(x: bounds.midX, y: 0)
        let bottom = bottomControlPointView.layer.presentation()?.position ?? CGPoint(x: bounds.midX, y: bounds.maxY)
        
        let bezierPath = UIBezierPath()
        bezierPath.move(to: CGPoint(x: bounds.minX + 0.05571 * bounds.width, y: bounds.minY + 0.00068 * bounds.height))
        bezierPath.addCurve(to: top, controlPoint1: CGPoint(x: bounds.minX + 0.05571 * bounds.width, y: bounds.minY + 0.00068 * bounds.height), controlPoint2: CGPoint(x: bounds.minX + 0.35957 * bounds.width, y: top.y))
        bezierPath.addCurve(to: CGPoint(x: bounds.minX + 0.94429 * bounds.width, y: bounds.minY + 0.00068 * bounds.height), controlPoint1: CGPoint(x: bounds.minX + 0.62694 * bounds.width, y: top.y), controlPoint2: CGPoint(x: bounds.minX + 0.94429 * bounds.width, y: bounds.minY + 0.00068 * bounds.height))
        bezierPath.addCurve(to: CGPoint(x: bounds.minX + 1.00000 * bounds.width, y: bounds.minY + 0.29758 * bounds.height), controlPoint1: CGPoint(x: bounds.minX + 0.97506 * bounds.width, y: bounds.minY + 0.00068 * bounds.height), controlPoint2: CGPoint(x: bounds.minX + 1.00000 * bounds.width, y: bounds.minY + 0.13361 * bounds.height))
        bezierPath.addLine(to: CGPoint(x: bounds.minX + 1.00000 * bounds.width, y: bounds.minY + 0.69990 * bounds.height))
        bezierPath.addCurve(to: CGPoint(x: bounds.minX + 0.94429 * bounds.width, y: bounds.minY + 0.99681 * bounds.height), controlPoint1: CGPoint(x: bounds.minX + 1.00000 * bounds.width, y: bounds.minY + 0.86388 * bounds.height), controlPoint2: CGPoint(x: bounds.minX + 0.97506 * bounds.width, y: bounds.minY + 0.99681 * bounds.height))
        bezierPath.addCurve(to: bottom, controlPoint1: CGPoint(x: bounds.minX + 0.94429 * bounds.width, y: bounds.minY + 0.99681 * bounds.height), controlPoint2: CGPoint(x: bounds.minX + 0.70588 * bounds.width, y: bottom.y))
        bezierPath.addCurve(to: CGPoint(x: bounds.minX + 0.05571 * bounds.width, y: bounds.minY + 0.99681 * bounds.height), controlPoint1: CGPoint(x: bounds.minX + 0.27083 * bounds.width, y: bottom.y), controlPoint2: CGPoint(x: bounds.minX + 0.05571 * bounds.width, y: bounds.minY + 0.99681 * bounds.height))
        bezierPath.addCurve(to: CGPoint(x: bounds.minX + 0.00000 * bounds.width, y: bounds.minY + 0.69990 * bounds.height), controlPoint1: CGPoint(x: bounds.minX + 0.02494 * bounds.width, y: bounds.minY + 0.99681 * bounds.height), controlPoint2: CGPoint(x: bounds.minX + 0.00000 * bounds.width, y: bounds.minY + 0.86388 * bounds.height))
        bezierPath.addLine(to: CGPoint(x: bounds.minX + 0.00000 * bounds.width, y: bounds.minY + 0.29758 * bounds.height))
        bezierPath.addCurve(to: CGPoint(x: bounds.minX + 0.05571 * bounds.width, y: bounds.minY + 0.00068 * bounds.height), controlPoint1: CGPoint(x: bounds.minX + 0.00000 * bounds.width, y: bounds.minY + 0.13361 * bounds.height), controlPoint2: CGPoint(x: bounds.minX + 0.02494 * bounds.width, y: bounds.minY + 0.00068 * bounds.height))
        bezierPath.close()

        
        return bezierPath.cgPath
    }
    
    @objc func updateLoop() {
        elasticShape.path = bezierPathForControlPoints()
    }
    
    private func startUpdateLoop() {
        displayLink.isPaused = false
    }
    
    private func stopUpdateLoop() {
        displayLink.isPaused = true
    }
    
    func animateControlPoints() {
        
        let overshootAmount = self.overshootAmount
        
        UIView.animate(withDuration: 0.25, delay: 0, usingSpringWithDamping: 0.1, initialSpringVelocity: 1.5, options: .curveEaseInOut, animations: {
            self.label.center.y += overshootAmount * 2
            self.topControlPointView.center.y += overshootAmount
            self.bottomControlPointView.center.y += overshootAmount
        }) { _ in
            
            UIView.animate(withDuration: 0.45, delay: 0.0, usingSpringWithDamping: 0.15, initialSpringVelocity: 5.5, options: .curveEaseInOut, animations: {
                self.positionControlPoints()
            }, completion: { _ in
                
                self.stopUpdateLoop()
                // call_back
                if let handler = self.animEnded {
                    handler()
                }
                
            })
            self.label.frame = self.bounds
            self.showLabel()
        }
        
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        hideLabel()
        startUpdateLoop()
        animateControlPoints()
    }
    
    func hideLabel() {
        UIView.animate(withDuration: 0.1) {
            self.label.alpha = 0
        }
    }
    
    func showLabel() {
        UIView.animate(withDuration: 0.6) {
            self.label.alpha = 1
        }
    }
}
