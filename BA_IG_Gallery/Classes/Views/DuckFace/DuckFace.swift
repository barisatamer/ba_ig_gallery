//
//  DuckFace.swift
//  BA_IG_Gallery
//
//  Created by Baris Atamer on 10/25/17.
//  Copyright © 2017 Baris Atamer. All rights reserved.
//

import UIKit

class DuckFace: UIView {

    let mouth = DuckMouth()
    let eyeRight = DuckEye()
    let eyeLeft = DuckEye()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupComponents()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setupComponents()
    }
    
    private func setupComponents() {
        addSubview(mouth)
        addSubview(eyeRight)
        addSubview(eyeLeft)
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        // Layout mouth
        mouth.frame = CGRect(x: 0, y: frame.size.height - frame.size.height/5, width: frame.size.width, height: frame.size.height/5)
        
        // Layout right eye
        let eyeSize = CGSize(width: 110, height: 110)
        eyeRight.frame = CGRect(x:frame.size.width - eyeSize.width * 1.2, y:eyeSize.height * 0.5, width:eyeSize.width, height:eyeSize.height)
        eyeLeft.frame = CGRect(x: eyeSize.width * 0.2, y:eyeSize.height * 0.5, width:eyeSize.width, height:eyeSize.height)
        
        animateEyes()
    }

    func animateEyes() {
            
        UIView.animate(withDuration: 2.25, delay: 0.1, usingSpringWithDamping: 0.1, initialSpringVelocity: 1.5, options: [.curveEaseInOut, UIViewAnimationOptions.autoreverse, UIViewAnimationOptions.repeat ], animations: {

            self.eyeLeft.transform = CGAffineTransform(scaleX: 0.85, y: 0.75)
            
        }) { _ in
        }
        
        UIView.animate(withDuration: 2.25, delay: 0.2, usingSpringWithDamping: 0.1, initialSpringVelocity: 1.5, options: [.curveEaseInOut, UIViewAnimationOptions.autoreverse, UIViewAnimationOptions.repeat ], animations: {
            
            self.eyeRight.transform = CGAffineTransform(scaleX: 0.85, y: 0.75)
            
        }) { _ in
        }
    }
    
    func stopAnimations() {
//        eyeLeft.removeAll
    }
}
