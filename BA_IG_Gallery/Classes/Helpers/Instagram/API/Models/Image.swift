import Foundation
import Gloss

public struct Image: Gloss.Decodable {
    
    public let width: Int?
    public let height: Int?
    public let url: String?
    
    public init?(json: JSON) {
        self.width = "width" <~~ json
        self.height = "height" <~~ json
        self.url = "url" <~~ json
    }
}
