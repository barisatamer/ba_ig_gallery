import Foundation
import Gloss

public struct Post: Gloss.Decodable {
    
    public let id: String?
    public let imageThumbnail: Image
    public let imageLowResolution: Image
    public let imageStandardResolution: Image
    public let likeCount: Int
    public let caption: Caption?
    
    // MARK: - Deserialization

    public init?(json: JSON) {
        
        // ID
        self.id = "id" <~~ json
        
        // Images
        guard let images: JSON = "images" <~~ json,
            let thumbnail: Image = "thumbnail" <~~ images,
            let lowResolution: Image = "low_resolution" <~~ images,
            let standardResolution: Image = "standard_resolution" <~~ images else {
            return nil
        }
        
        // Like count
        guard let likes: JSON = "likes" <~~ json,
            let likeCount: Int = "count" <~~ likes else {
            return nil
        }
        
        // Caption
        self.caption = "caption" <~~ json
        
        self.likeCount = likeCount
        self.imageThumbnail = thumbnail
        self.imageLowResolution = lowResolution
        self.imageStandardResolution = standardResolution
    }
    
}
