import Foundation
import Gloss

public struct RecentPosts: Gloss.Decodable {
    
    public let posts: [Post]?
    public let pagination: Pagination?
    
    public init?(json: JSON) {
        posts = "data" <~~ json
        pagination = "pagination" <~~ json
    }
}
