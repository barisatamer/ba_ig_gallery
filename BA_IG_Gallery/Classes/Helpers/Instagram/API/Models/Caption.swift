import Foundation
import Gloss

public struct Caption: Gloss.Decodable {
    
    public let text: String?
    public let createdTime: String?
    
    public init?(json: JSON) {
        
        self.text = "text" <~~ json
        self.createdTime = "created_time" <~~ json
    }
}

