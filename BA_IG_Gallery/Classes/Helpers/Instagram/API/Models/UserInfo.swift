import Foundation
import Gloss

public struct UserInfo: Gloss.Decodable {
    
    public let id: String?
    public let username: String?
    public let profilePicture: String?
    public let fullName: String?
    public let bio: String?
    public let website: String?
    public let isBusiness: Bool?
    
    public let mediaCount: Int?
    public let followsCount: Int?
    public let followedByCount: Int?
    
    public init?(json: JSON) {
        
        guard let container:JSON = "data" <~~ json else {
            return nil
        }
        
        self.id = "id" <~~ container
        self.username = "username" <~~ container
        self.profilePicture = "profile_picture" <~~ container
        self.fullName = "full_name" <~~ container
        self.bio = "bio" <~~ container
        self.website = "website" <~~ container
        self.isBusiness = "is_business" <~~ container
        
        guard let countsCountainer:JSON = "counts" <~~ container else {
            return nil
        }
        self.mediaCount = "media" <~~ countsCountainer
        self.followsCount = "follows" <~~ countsCountainer
        self.followedByCount = "followed_by" <~~ countsCountainer
    }
}

