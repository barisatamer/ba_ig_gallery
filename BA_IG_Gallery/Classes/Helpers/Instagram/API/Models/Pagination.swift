//
//  Pagination.swift
//  instagram_posts_test
//
//  Created by Baris Atamer on 10/22/17.
//  Copyright © 2017 Baris Atamer. All rights reserved.
//

import Foundation
import Gloss

public struct Pagination: Gloss.Decodable {
    
    public let nextMaxId: String?
    public let nextUrl: String?
    
    public init?(json: JSON) {
        self.nextMaxId = "next_max_id" <~~ json
        self.nextUrl = "next_url" <~~ json
    }
}
