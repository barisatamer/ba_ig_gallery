//
//  Result.swift
//  BA_IG_Gallery
//
//  Created by Baris Atamer on 10/23/17.
//  Copyright © 2017 Baris Atamer. All rights reserved.
//

enum Result<T, U> where U: Error {
    case success(payload: T)
    case failure(U?)
}
