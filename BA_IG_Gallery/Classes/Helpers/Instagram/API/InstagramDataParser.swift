//
//  InstagramDataParser.swift
//  BA_IG_Gallery
//
//  Created by Baris Atamer on 10/23/17.
//  Copyright © 2017 Baris Atamer. All rights reserved.
//

import Foundation

public class InstagramDataParser {

    public class func parseUser(json: Any?) -> UserInfo? {
        
        guard let dictionary = json as? [String: Any] else {
            print("Error in parsing UserInfo")
            return nil
        }
        
        guard let userInfo = UserInfo(json: dictionary) else {
            print("Error initializing object")
            return nil
        }
        
        return userInfo;
    }
    
    public class func parseRecentPosts(json: Any?) -> RecentPosts? {
        
        guard let dictionary = json as? [String: Any] else {
            print("Error in parsing UserInfo")
            return nil
        }
        
        guard let recentPosts = RecentPosts(json: dictionary) else {
            print("Error initializing object")
            return nil
        }
        
        return recentPosts;
    }
}
