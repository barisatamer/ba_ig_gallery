//
//  InstagramAPIConstants.swift
//  BA_IG_Gallery
//
//  Created by Baris Atamer on 10/22/17.
//  Copyright © 2017 Baris Atamer. All rights reserved.
//

import Foundation

struct INSTAGRAM_API{
    static let CONFIG_KEY_CLIENT_ID = "InstagramAppClientId"
    static let CONFIG_KEY_REDIRECT_URL = "InstagramAppRedirectURL"
    static let CONFIG_KEY_ACCESS_TOKEN = "ig_auth_token" // to be used for KeyChain access
    
    static let INSTAGRAM_AUTHURL = "https://api.instagram.com/oauth/authorize/"
    static let FETCH_USER_INFO = "https://api.instagram.com/v1/users/self/?access_token="
    static let FETCH_RECENT_POSTS = "https://api.instagram.com/v1/users/self/media/recent/?count=20&access_token="
}
