//
//  InstagramEngine.swift
//  BA_IG_Gallery
//
//  Created by Baris Atamer on 10/22/17.
//  Copyright © 2017 Baris Atamer. All rights reserved.
//

import UIKit
import KeychainSwift
import Alamofire
import Gloss

class InstagramEngine {

    static let sharedInstance = InstagramEngine()
    
    var appClientID: String
    var appRedirectURL: String
    
    var accessToken: String?
    
    private init() {
        
        // Extract client id and redirect url from Info.plist
        if let infoPlist = Bundle.main.infoDictionary {
            
            if let clientId = infoPlist[INSTAGRAM_API.CONFIG_KEY_CLIENT_ID] as? String {
                self.appClientID = clientId
            } else {
                fatalError("Invalid Client ID. Please set a valid value for the key \(INSTAGRAM_API.CONFIG_KEY_CLIENT_ID) in the App's Info.plist file")
            }
            
            if let redirectURL = infoPlist[INSTAGRAM_API.CONFIG_KEY_REDIRECT_URL] as? String {
                self.appRedirectURL = redirectURL
            } else {
                fatalError("Invalid Redirect URL. Please set a valid value for the key \(INSTAGRAM_API.CONFIG_KEY_REDIRECT_URL) in the App's Info.plist file")
            }
        } else {
            fatalError("Error in reading Info.plist")
        }
        
        // Get access_token
        let keychain = KeychainSwift()
        accessToken = keychain.get(INSTAGRAM_API.CONFIG_KEY_ACCESS_TOKEN)
    }
    
    func authUrl() -> URL {
        let scope = "follower_list+public_content"
        let authURL = String(format: "%@?client_id=%@&redirect_uri=%@&response_type=token&scope=%@&DEBUG=True", arguments: [INSTAGRAM_API.INSTAGRAM_AUTHURL,appClientID, appRedirectURL, scope])
        
        let url = URL.init(string: authURL)!
        return url
    }
    
    func setAccessToken(token: String) {
        let keychain = KeychainSwift()
        keychain.set(token, forKey: INSTAGRAM_API.CONFIG_KEY_ACCESS_TOKEN)
        accessToken = token
    }
    
    func isSessionValid() -> Bool {
        return accessToken != nil
    }
    
    func logout() {
        let keychain = KeychainSwift()
        keychain.delete(INSTAGRAM_API.CONFIG_KEY_ACCESS_TOKEN)
        accessToken = nil
    }
    
    // MARK: - Fetching API
    enum IGFetchFailure: Int, Error {
        case unAuthorized = 401
        case notFound = 404
    }
    
    typealias InstagramUserInfoFetchResult = Result<UserInfo, IGFetchFailure>
    typealias InstagramUserInfoFetchCompletion = (_ result: InstagramUserInfoFetchResult) -> Void
    
    func fetchUserInfo(completion: @escaping InstagramUserInfoFetchCompletion) {
        Alamofire.request(INSTAGRAM_API.FETCH_USER_INFO + accessToken!)
            .validate()
            .responseJSON { response in
                
                switch response.result {
                case .success:
                    guard let userInfo: UserInfo = InstagramDataParser.parseUser(json: response.result.value) else {
                        completion(.failure(nil))
                        return
                    }
                    completion(InstagramEngine.InstagramUserInfoFetchResult.success(payload: userInfo))
                case .failure(_):
                    if let statusCode = response.response?.statusCode,
                        let reason = IGFetchFailure(rawValue: statusCode) {
                        completion(.failure(reason))
                        return
                    }
                    completion(.failure(nil))
                }
        }
    }
    
    typealias InstagramRecentPostsFetchResult = Result<[Post], IGFetchFailure>
    typealias InstagramRecentPostsFetchCompletion = (_ result: InstagramRecentPostsFetchResult) -> Void
    var recentPostsNextUrl: String?
    func fetchRecentPost(completion: @escaping InstagramRecentPostsFetchCompletion) {
        
        var url = INSTAGRAM_API.FETCH_RECENT_POSTS + accessToken!
        
        if let nextUrl = self.recentPostsNextUrl {
            url = nextUrl + accessToken!
        }
        
        Alamofire.request(url)
            .validate()
            .responseJSON { [weak self] response in
                
                switch response.result {
                case .success:
                    guard let recentPost: RecentPosts = InstagramDataParser.parseRecentPosts(json: response.result.value) else {
                        completion(.failure(nil))
                        return
                    }
                    
                    if let nextUrl = recentPost.pagination?.nextUrl {
                        self?.recentPostsNextUrl = nextUrl
                    }
                    guard let posts: [Post] = recentPost.posts else {
                        completion(.failure(nil))
                        return
                    }
                    completion(InstagramEngine.InstagramRecentPostsFetchResult.success(payload: posts))
                    
                case .failure(_):
                    if let statusCode = response.response?.statusCode,
                        let reason = IGFetchFailure(rawValue: statusCode) {
                        completion(.failure(reason))
                        return
                    }
                    completion(.failure(nil))
                }
        }
    }
}
