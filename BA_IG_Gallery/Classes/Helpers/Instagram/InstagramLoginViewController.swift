//
//  InstagramLoginViewController.swift
//  iGenius IG Gallery
//
//  Created by Baris Atamer on 10/19/17.
//  Copyright © 2017 Baris Atamer. All rights reserved.
//

import UIKit
import WebKit

protocol InstagramAuthDelegate {
    func authControllerDidFinish(accessToken: String?, error: NSError?)
}

class InstagramLoginViewController: UIViewController, WKNavigationDelegate {

    // MARK: - Properties
    
    var webView: WKWebView!
    var delegate: InstagramAuthDelegate?
    var authURL: URL?
    
    var progressView: UIProgressView!
    
    
    // MARK: - View Lifecycle
    
    override func loadView() {
        let webConfiguration = WKWebViewConfiguration()
        if #available(iOS 9.0, *) {
            webConfiguration.websiteDataStore = WKWebsiteDataStore.nonPersistent()
        }
        webView = WKWebView(frame: .zero, configuration: webConfiguration)
        webView.navigationDelegate = self
        view = webView
        self.title = "Login"
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        initItems()
        webView.addObserver(self, forKeyPath: #keyPath(WKWebView.estimatedProgress), options: .new, context: nil)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.isToolbarHidden = false
        
        if let authURL = self.authURL {
            let urlRequest = URLRequest.init(url: authURL)
            webView.load(urlRequest)
        } else {
            print("Authorization URL is not set")
            dismiss()
        }
    }
    
    func initItems() {
        navigationItem.leftBarButtonItem = UIBarButtonItem(barButtonSystemItem: .stop, target: self, action: #selector(closeTapped))
        navigationItem.rightBarButtonItem = UIBarButtonItem(barButtonSystemItem: .refresh, target: webView, action: #selector(webView.reload))
        
        // Initialize the Progress View
        progressView = UIProgressView(progressViewStyle: .bar)
        progressView.frame = CGRect(x: 0, y: 0, width: UIScreen.main.bounds.size.width, height: 20)
        // Toolbar items
        let progressButton = UIBarButtonItem(customView: progressView)
        toolbarItems = [progressButton]
    }
    
    @objc func closeTapped() {
        dismiss()
    }
    
    deinit {
        webView.removeObserver(self, forKeyPath: #keyPath(WKWebView.estimatedProgress))
    }
    
    // MARK: - UIWebViewDelegate
    
    func webView(_ webView: WKWebView, decidePolicyFor navigationAction: WKNavigationAction, decisionHandler: @escaping (WKNavigationActionPolicy) -> Void) {
        if !checkRequestForCallbackURL(request: navigationAction.request) {
            decisionHandler(.cancel)
        }
        else {
            decisionHandler(.allow)
        }
    }
    
    override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
        
        if keyPath == "estimatedProgress" {
            progressView.progress = Float(webView.estimatedProgress)
            progressView.isHidden = progressView.progress == 1
        }
    }
    
    // MARK: - Private methods
    
    func checkRequestForCallbackURL(request: URLRequest) -> Bool {
        let requestURLString = (request.url?.absoluteString)! as String
        
        if requestURLString.hasPrefix(InstagramEngine.sharedInstance.appRedirectURL) {
            let range: Range<String.Index> = requestURLString.range(of: "#access_token=")!
            handleAuth(authToken: String(requestURLString[range.upperBound...]))
            return false;
        }
        return true
    }
    
    func handleAuth(authToken: String)  {
        guard let delegate = self.delegate else {
            fatalError("InstagramAuthDelegate method needs to be implemented")
        }
        InstagramEngine.sharedInstance.setAccessToken(token: authToken)
        delegate.authControllerDidFinish(accessToken: authToken, error: nil)
        dismiss()
    }
    
    private func dismiss() {
        OperationQueue.main.addOperation {
            self.dismiss(animated: true, completion: nil)
        }
    }
}
