//
//  UIView+Shadow.swift
//  BA_IG_Gallery
//
//  Created by Baris Atamer on 10/26/17.
//  Copyright © 2017 Baris Atamer. All rights reserved.
//

import UIKit

extension UIView {
    
    func addShadow() {
        layer.shadowOffset = CGSize(width: 5, height: 5)
        layer.shadowRadius = 10
        layer.shadowOpacity = 0.5
        layer.shadowColor = UIColor.black.cgColor
    }
    
    func addStroke() {
        layer.borderWidth = 5
        layer.borderColor = UIColor.white.cgColor
    }
}

