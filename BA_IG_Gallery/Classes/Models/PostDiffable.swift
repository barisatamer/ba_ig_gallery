//
//  PostDiffable.swift
//  BA_IG_Gallery
//
//  Created by Baris Atamer on 10/24/17.
//  Copyright © 2017 Baris Atamer. All rights reserved.
//

import Foundation
import IGListKit

final class PostDiffable {
    
    public let post: Post
    
    init(post: Post) {
        self.post = post
    }
}

extension PostDiffable: ListDiffable {

    func diffIdentifier() -> NSObjectProtocol {
        return post.id! as NSObjectProtocol
    }
    
    func isEqual(toDiffableObject object: ListDiffable?) -> Bool {
        return true
    }
}

