//
//  IG_ClientTests.swift
//  BA_IG_GalleryTests
//
//  Created by Baris Atamer on 10/25/17.
//  Copyright © 2017 Baris Atamer. All rights reserved.
//

import XCTest
import OHHTTPStubs

@testable import BA_IG_Gallery

class IG_ClientTests: XCTestCase {
    
    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    

    /// Test Client's User Info method with a valid response and status code 200 and a failure with 404
    func testApiClientUserInfo() {
        
        // **** 200 ****
        let stubPath = OHPathForFile("user_info.json", type(of: self))
        let usersStub = stub(condition: isMethodGET() ) { (request) -> OHHTTPStubsResponse in
            return OHHTTPStubsResponse(fileAtPath: stubPath!, statusCode: 200, headers: ["Content-Type":"application/json"])
        }
        let expectation = self.expectation(description: "waits for the client request 200")
        InstagramEngine.sharedInstance.fetchUserInfo(completion: { result in
            
            switch result {
            case .success(let userInfo):
                XCTAssertEqual(userInfo.id!, "6994064", "User id is not 6994064")
            case .failure(let error):
                if let err = error {
                    XCTFail("\(err)")
                } else {
                    XCTFail("Unknown failure")
                }
            }
            expectation.fulfill()
        })
        waitForExpectations(timeout: 2, handler: nil)
        OHHTTPStubs.removeStub(usersStub)
        
        
        // **** 404 ****
        let usersStub404 = stub(condition: isMethodGET() ) { (request) -> OHHTTPStubsResponse in
            return OHHTTPStubsResponse(fileAtPath: stubPath!, statusCode: 404, headers: ["Content-Type":"application/json"])
        }
        let expectation2 = self.expectation(description: "waits for the client request 404")
        InstagramEngine.sharedInstance.fetchUserInfo(completion: { result in
            
            switch result {
            case .success(_):
                XCTFail("Unexpected result")
            case .failure(let error):
                if let err = error {
                    XCTAssertEqual(404, err.rawValue, "Error code is not 404")
                } else {
                    XCTFail("Unknow error code")
                }
            }
            expectation2.fulfill()
        })
        waitForExpectations(timeout: 2, handler: nil)
        OHHTTPStubs.removeStub(usersStub404)
    }
    
    func testApiClientRecentPost() {
        
        InstagramEngine.sharedInstance.accessToken = "fake"
        
        // **** 200 ****
        let stubPath = OHPathForFile("recent_media.json", type(of: self))
        let usersStub200 = stub(condition: isMethodGET() ) { (request) -> OHHTTPStubsResponse in
            return OHHTTPStubsResponse(fileAtPath: stubPath!, statusCode: 200, headers: ["Content-Type":"application/json"])
        }
        let expectation = self.expectation(description: "waits for the client request 200")
        InstagramEngine.sharedInstance.fetchRecentPost(completion: { result in
            
            switch result {
            case .success(let resultPosts):
                
                if let post = resultPosts.first {
                    XCTAssertEqual(post.caption?.text, "Spread your wings", "Unexpected value for the first post.")
                } else {
                    XCTFail("Failed getting post")
                }
                
            case .failure(let error):
                if let err = error {
                    XCTFail("\(err)")
                } else {
                    XCTFail("Unknown failure")
                }
            }
            expectation.fulfill()
        })
        waitForExpectations(timeout: 2, handler: nil)
        OHHTTPStubs.removeStub(usersStub200)
        
        
        // **** 404 ****
        let usersStub404 = stub(condition: isMethodGET() ) { (request) -> OHHTTPStubsResponse in
            return OHHTTPStubsResponse(fileAtPath: stubPath!, statusCode: 404, headers: ["Content-Type":"application/json"])
        }
        let expectation2 = self.expectation(description: "waits for the client request 404")
        InstagramEngine.sharedInstance.fetchRecentPost(completion: { result in
            
            switch result {
            case .success(_):
                XCTFail("Unexpected result")
            
            case .failure(let error):
                if let err = error {
                    XCTAssertEqual(404, err.rawValue, "Error code is not 404")
                } else {
                    XCTFail("Unknow error code")
                }
            }
            expectation2.fulfill()
        })
        waitForExpectations(timeout: 2, handler: nil)
        OHHTTPStubs.removeStub(usersStub404)
        
    }
    
    
    
    
}

