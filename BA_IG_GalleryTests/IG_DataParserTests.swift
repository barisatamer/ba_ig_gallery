//
//  IG_DataParserTests.swift
//  BA_IG_GalleryTests
//
//  Created by Baris Atamer on 10/23/17.
//  Copyright © 2017 Baris Atamer. All rights reserved.
//

import XCTest
@testable import BA_IG_Gallery

class IG_DataParserTests: XCTestCase {
    
    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testUser() {
        let bundle = Bundle(for: type(of: self))
        
        guard let filePath = bundle.path(forResource: "user_info", ofType: "json") else {
            XCTFail("Missing file: user_info.json")
            return
        }
        
        let data = try! Data(contentsOf: URL(fileURLWithPath:filePath), options: .uncached)
        
        var json: Any
        do {
            json = try JSONSerialization.jsonObject(with: data)
        } catch {
            print(error)
            return
        }
        
        let userInfo: UserInfo? = InstagramDataParser.parseUser(json: json)
        XCTAssertEqual(userInfo?.username!, "barisatamer", "User name is not barisatamer")
        XCTAssertEqual(userInfo?.id!, "6994064", "User id is not 6994064")
        XCTAssertEqual(userInfo?.mediaCount!, 161, "Media count is not 161")
        XCTAssertEqual(userInfo?.followsCount!, 289, "Follows count is not 289")
        XCTAssertEqual(userInfo?.followedByCount!, 965, "Followed by count is not 965")
    }
    
    func testRecentPosts() {
        let bundle = Bundle(for: type(of: self))
        
        guard let filePath = bundle.path(forResource: "recent_media", ofType: "json") else {
            XCTFail("Missing file: recent_media.json")
            return
        }
        
        let data = try! Data(contentsOf: URL(fileURLWithPath:filePath), options: .uncached)
        
        var json: Any
        do {
            json = try JSONSerialization.jsonObject(with: data)
        } catch {
            print(error)
            return
        }
        
        let recentPosts = InstagramDataParser.parseRecentPosts(json: json)
        let firstPost = recentPosts?.posts?.first
        let lastPost = recentPosts?.posts?.last
        
        // First post
        XCTAssertEqual(firstPost?.likeCount, 256, "Like count is not 256")
        XCTAssertEqual(firstPost?.imageLowResolution.url, "https://scontent.cdninstagram.com/t51.2885-15/s320x320/e35/21910971_128454124469772_6323703585120976896_n.jpg", "Low resolution image url is incorrect")
        XCTAssertEqual(firstPost?.imageThumbnail.url, "https://scontent.cdninstagram.com/t51.2885-15/s150x150/e35/c43.0.993.993/21910971_128454124469772_6323703585120976896_n.jpg", "Thumbnail image url is incorrect")
        XCTAssertEqual(firstPost?.imageStandardResolution.url, "https://scontent.cdninstagram.com/t51.2885-15/s640x640/sh0.08/e35/21910971_128454124469772_6323703585120976896_n.jpg", "Standard image url is incorrect")
        XCTAssertEqual(firstPost?.caption?.text, "Spread your wings", "Caption is not Spread your wings")
        
        // Last post
        XCTAssertEqual(lastPost?.likeCount, 179, "Like count is not 179")
        XCTAssertEqual(lastPost?.imageLowResolution.url, "https://scontent.cdninstagram.com/t51.2885-15/s320x320/e35/19428978_1942933205990464_4872713794008121344_n.jpg", "Low resolution image url is incorrect")
        XCTAssertEqual(lastPost?.imageThumbnail.url, "https://scontent.cdninstagram.com/t51.2885-15/s150x150/e35/c135.0.810.810/19428978_1942933205990464_4872713794008121344_n.jpg", "Thumbnail image url is incorrect")
        XCTAssertEqual(lastPost?.imageStandardResolution.url, "https://scontent.cdninstagram.com/t51.2885-15/s640x640/sh0.08/e35/19428978_1942933205990464_4872713794008121344_n.jpg", "Standard image url is incorrect")
        XCTAssertEqual(lastPost?.caption?.text, nil, "Caption is not nil")
    }
    
}
