# BA Instagram Gallery App


[![Screen recording](https://img.youtube.com/vi/zhVkd3L3kco/0.jpg)](https://youtu.be/zhVkd3L3kco   "Screen recording")

A photo gallery application using Instagram API, populated with photos, published on Instagram by the user that logged in from the app.


## Requirements

- iOS 9.3+
- Xcode 9.0+
- Swift 4.0+

## Building

### Instagram Client
Register a new client at [Instagram Developer page](https://www.instagram.com/developer/clients/manage/) and copy the **Client** ID and **Redirect URL**

 into
    ```InstagramAppClientId``` and
    ```InstagramAppRedirectURL ``` in **Info.plist**


## Dependencies

* KeychainSwift
* Alamofire
* AlamofireImage
* Gloss
* IGListKit
* ElasticTransition
* R.swift
* ReachabilitySwift
* Floaty
* OHHTTPStubs/Swift